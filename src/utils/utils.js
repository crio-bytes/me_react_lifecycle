export const LOGS = 'logs'
const CAPTURE = 'capture'

export const capture = () => {
    if (!localStorage.getItem(CAPTURE)) {
        return false
    }
    return Boolean(parseInt(localStorage.getItem(CAPTURE)))
}

export const getLogs = () => {
    if (!localStorage.getItem(LOGS)) {
        return []
    }
    return filterLogs(JSON.parse(localStorage.getItem(LOGS)))
}
const filterLogs = (logs) => {
    let newLogs = [logs[0]]

    for (let i = 1 ; i < logs.length ; ++i) {
        if (JSON.stringify(logs[i]) === JSON.stringify(logs[i-1])) {
            continue
        }
        newLogs.push(logs[i])
    }
    return newLogs
}

export const initialize = () => {
    if (!localStorage.getItem(CAPTURE)) {
        localStorage.setItem(CAPTURE, '0')
    }
    localStorage.setItem(LOGS, JSON.stringify([]))
}

export const toggleCapture = () => {
    let logs = []
    if (capture()) {
        logs = getLogs()
    }

    let toggledCapture = !capture()
    toggledCapture = (toggledCapture) ? '1' : '0';
    localStorage.setItem(CAPTURE, toggledCapture)
    localStorage.setItem(LOGS, JSON.stringify([]))

    return logs
}

export const pushLogs = (log) => {
    if (capture()) {
        let logs = getLogs()
        logs.push(log)
        localStorage.setItem(LOGS, JSON.stringify(logs))
    }
}
