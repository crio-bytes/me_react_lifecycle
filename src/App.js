import React, { Component } from "react";
import "./App.css";
import {
  capture,
  getLogs,
  initialize,
  LOGS,
  toggleCapture,
} from "./utils/utils";
import Logs from "./components/Logs";
import Main from "./components/Main";

class App extends Component {
  constructor(props) {
    super(props);
    initialize();
    this.state = {
      capture: capture(),
      allLogs: getLogs(),
      logs: [],
      index: 1,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.capture !== this.state.capture) {
      localStorage.setItem(LOGS, JSON.stringify([]));
    }
  }

  onCaptureClick = () => {
    let logs = toggleCapture();
    this.setState({ capture: capture() });

    if (this.state.capture) {
      this.setState({
        allLogs: logs,
      });

      const intervalId = setInterval(() => {
        console.log(this.state.index);
        this.setState({
          logs: this.state.allLogs.slice(1, this.state.index),
          index: this.state.index + 1,
        });
        if (this.state.index - 1 === this.state.allLogs.length) {
          clearInterval(intervalId);
        }
      }, 1000); // TODO: Update this value to change the log printing interval eg: 1000 = 1 second
    } else {
      this.setState({
        index: 0,
        logs: [],
        allLogs: [],
      });
    }
  };

  render() {
    return (
      <div className="container">
        <Main />
        <button onClick={this.onCaptureClick}>
          {!this.state.capture ? "Start" : "Stop"}
        </button>
        <Logs logs={this.state.logs} />
      </div>
    );
  }
}

export default App;
