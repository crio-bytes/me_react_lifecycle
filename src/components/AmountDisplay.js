import React from "react";
import "./AmountDisplay.css";
function AmountDisplay(props) {
  return (
    <div className="amount-display-container">
      <h3>{props.text}</h3>
      <h3>₹ {props.amount}</h3>
    </div>
  );
}

export default AmountDisplay;
