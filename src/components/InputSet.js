import React, { Component } from "react";
import "./InputSet.css";
import InputField from "./InputField";
import { pushLogs } from "../utils/utils";
import AmountDisplay from "./AmountDisplay";

export default class InputSet extends Component {
  constructor(props) {
    super(props);
    pushLogs(`${this.props.name}: constructor`);
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    pushLogs(`${this.props.name}: shouldComponentUpdate`);
    return true;
  }

  componentDidMount() {
    pushLogs(`${this.props.name}: componentDidMount`);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    pushLogs(`${this.props.name}: componentDidUpdate`);
  }

  render() {
    pushLogs(`${this.props.name}: render`);

    let total = this.getSum(this.props.children);
    return (
      <div>
        <AmountDisplay text={this.props.heading} amount={total} />
        <div className="set-container">{this.props.children}</div>
      </div>
    );
  }

  getSum = (children) => {
    let totalSum = React.Children.map(children, (child) => {
      if (child.type === InputField) {
        return parseInt(child.props.value);
      } else if (child.type === InputSet) {
        return this.getSum(child.props.children);
      }
    });
    return totalSum.reduce((a, b) => a + b, 0);
  };
}
