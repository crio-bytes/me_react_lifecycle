import React, {Component} from 'react';
import './Logs.css'
export default class Logs extends Component {

    render() {
        let logs = this.props.logs;
        let id = 0
        let groupedLogs = logs.reduce((grouped, obj) => {
            if (!obj) {
                return grouped
            }
            let [name, hook] = obj.split(':')
            if (!grouped[name]) {
                grouped[name] = []
            }
            grouped[name].push(<p key={id} className={'log-hook'}>{hook}</p>)
            id++
            return grouped
        }, {})

        let logElement = [];
        id=0
        for (const name in groupedLogs) {
            id++
            logElement.push((<div key={id} className={'log-container'}>
                                <h3>{name}:</h3>
                                {groupedLogs[name]}
                            </div>)
            )
        }

        return (
            <div>
                {logElement}
            </div>
        )
    }
}
