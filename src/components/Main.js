import React, { Component } from "react";
import "../App.css";
import InputField from "./InputField";
import InputSet from "./InputSet";
import AmountDisplay from "./AmountDisplay";

class Main extends Component {
  state = {
    gross_salary_income: 0,
    interest_income: 0,
    property_income: 0,
    basic_deduction: 0,
    insurance_deduction: 0,
    charity_deduction: 0,
    loan_deduction: 0,
  };

  onChange = (event) => {
    let newValue = event.target.value;
    if (!newValue) {
      newValue = "0";
    }
    this.setState({
      [event.target.name]: newValue,
    });
  };

  render() {
    let taxableIncome =
      parseInt(this.state.gross_salary_income) +
      parseInt(this.state.interest_income) +
      parseInt(this.state.property_income) -
      parseInt(this.state.basic_deduction) -
      parseInt(this.state.charity_deduction) -
      parseInt(this.state.insurance_deduction) -
      parseInt(this.state.loan_deduction);
    return (
      <div>
        <AmountDisplay text="Net Taxable Income" amount={taxableIncome} />
        <InputSet name="income" heading="Total Income">
          <InputField
            name="gross_salary_income"
            heading="Gross Salary Income"
            meta="Amount Earned from salary"
            value={this.state.gross_salary_income}
            onChange={this.onChange}
          />
        </InputSet>
      </div>
    );
  }
}

export default Main;
