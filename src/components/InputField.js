import React, { Component } from "react";
import "./InputField.css";
import { pushLogs } from "../utils/utils";

export default class InputField extends Component {
  constructor(props) {
    super(props);
    pushLogs(`${this.props.name}: constructor`);
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    pushLogs(`${this.props.name}: shouldComponentUpdate`);
    return true;
  }

  componentDidMount() {
    pushLogs(`${this.props.name}: componentDidMount`);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    pushLogs(`${this.props.name}: componentDidUpdate`);
  }

  render() {
    pushLogs(`${this.props.name}: render`);

    return (
      <div className="input-field-container">
        <div className="text-container">
          <h3>{this.props.heading}</h3>
          <p>{this.props.meta}</p>
        </div>
        <div>
          ₹{" "}
          <input
            className="input-field"
            name={this.props.name}
            type="number"
            onChange={this.props.onChange}
          />
        </div>
      </div>
    );
  }
}
